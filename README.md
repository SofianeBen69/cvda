Alice : Sofiane Benslimane sofiane.benslimane@etu.univ-lyon1.fr

Bob : Alice Gaudon alice@gaudon.pro

Pour les merge requests, nous avons choisis de faire les merges en local, car l'outil web ne permettait pas une édition précise et le code résultant ne nous convenait pas.

Il nous a semblé plus simple de procéder à la ligne de commande.
