package test;

import entree.Genre;
import entree.Personne;
import entree.Societe;

public class TestPersonne {
    public static void main(String args[]) {

        String[] tab = {"Thierry", "Renauld"};
        Societe s1 = new Societe("lucratif");
        Personne p1 = new Personne("Jean", tab, Genre.HOMME, null, s1, "neutre");
        System.out.println(p1);

    }
}
