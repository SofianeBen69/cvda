package test;

import entree.Societe;

public class TestSociete {

    public static void main(String[] args) {
        Societe societe = new Societe("Bonjour");
        print(societe.toString(null, null).equals("[Societe] raisonSociale : Bonjour"));
    }

    private static void print(boolean expression) {
        System.out.println(expression ? "pass" : "fail");
    }
}
