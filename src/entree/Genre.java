package entree;

/**
 * For the "other" case, please make a custom named Genre : {@code new Genre("the name");}
 */
public class Genre {
    public static final Genre HOMME = new Genre("Homme");
    public static final Genre FEMME = new Genre("Femme");
    public static final Genre NEUTRE = new Genre("Neutre");

    private final String name;

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static Genre valueOf(String val) {
        switch (val) {
            case "Homme":
                return HOMME;
            case "Femme":
                return FEMME;
            case "Neutre":
                return NEUTRE;
            default:
                return new Genre(val);
        }
    }
}
