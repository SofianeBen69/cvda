package entree;

public class Personne implements Entree {
    String nom;
    String prenoms[];
    Genre genre;
    Personne conjoint;
    Societe societe;
    String fonction;


    public Personne(String nom, String[] prenoms, Genre genre, Personne conjoint, Societe societe, String fonction) {
        this.nom = nom;
        this.prenoms = prenoms;
        this.genre = genre;
        this.conjoint = conjoint;
        this.societe = societe;
        this.fonction = fonction;
    }

    @Override
    public String toString(Presentation presentation, Sens sens) {
        String out = "";
        boolean first = true;
        for (String prenom : prenoms) {
            if (presentation == Presentation.ABREGE) out += prenom.charAt(0) + ". ";
            if (presentation == Presentation.SIMPLE) {
                if (first) {
                    out = out + prenom + " ";
                    first = false;
                } else {
                    out = out + " " + prenom.charAt(0) + ".";
                }
            }
            if (presentation == Presentation.COMPLET) {
                out = out + " " + prenom;
            }
        }


        if (sens == Sens.NOMS_PRENOMS) {
            if ((presentation) == Presentation.ABREGE) return nom + out;
            else if ((presentation) == Presentation.SIMPLE) return nom + out + "(" + societe + ")";
            else if ((presentation) == Presentation.COMPLET)
                return nom + out + "\n" + "Societé : " + societe + "\n" + "-Fonction" + fonction;
        } else {
            if ((presentation) == Presentation.ABREGE) return out + nom;
            else if ((presentation) == Presentation.SIMPLE) return out + nom + "(" + societe + ")";
            else if ((presentation) == Presentation.COMPLET)
                return out + nom + "\n" + "Societé : " + societe + "\n" + "-Fonction" + fonction;
        }

        return out;
    }

    @Override
    public boolean recherche(String term) {
        return false;                              // Todo: a implementer retourner vrai si la chaine de caractère est contenue dans l’un des prénoms ou dans le nom.
    }
}
