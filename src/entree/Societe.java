package entree;

public class Societe implements Entree {
    private String raisonSociale;

    public Societe(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    @Override
    public String toString(Presentation presentation, Sens sens) {
        return "[" + this.getClass().getSimpleName() + "] raisonSociale : " + raisonSociale;
    }

    @Override
    public boolean recherche(String term) {
        return false; // TODO : retourne true si le champs raison sociale est trouvé.
    }
}
