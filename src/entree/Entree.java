package entree;

public interface Entree {
    String toString(Presentation presentation, Sens sens);

    boolean recherche(String term);
}
