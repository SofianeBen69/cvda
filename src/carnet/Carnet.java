package carnet;

import entree.*;

import java.util.ArrayList;

public class Carnet {
    Entree[] selectionnees;
    private ArrayList<Entree> entrees;

    public Carnet(Entree[] selectionnees) {
        this.selectionnees = selectionnees;
        this.entrees = new ArrayList<>();
    }

    public void ajouterEntree(Entree e1) {
        entrees.add(e1);
    }

    public void selection(String s1) {
        System.out.println(s1);
    }

    public void selection(Entree[] e1) {
        System.out.println(e1);
    }

    // TODO: finish this method
    public void lectureFichier(String content) {
        for (String entry : content.split("\n")) {
            String[] fields = entry.split(";");
            if (fields.length < 2) continue;
            if (fields[1].equalsIgnoreCase("PERSONNE") && fields.length >= 8) {
                this.entrees.add(new Personne(fields[2], fields[3].split(","), Genre.valueOf(fields[4]), null, null, fields[7]));
            }
        }
    }

    public void selection(Entree e1) {
        System.out.println(e1);
    }

    public void affichage(Ordre ordre, Presentation presentation, Sens sens) {
        // TODO: Implement this method
    }

    public void affichageSelection(Ordre ordre, Presentation presentation, Sens sens) {
        // TODO: Implement this method
    }
}
